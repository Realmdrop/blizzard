/*using System.Collections.Generic;
using System;
public class Player
{
    // Id in the event of name duplications
    public Guid Id { get; private set; } = Guid.NewGuid();

    // Name
    public string Name { get; set; } = "Unnamed";

    // Step overrides
    public Dictionary<string, int> StepOverrides { get; set; } = new Dictionary<string, int>();

    // Easy accessor so we don't have to re-run map again
    public int CachedTravelCost { get; private set; } = -1;

    public int TravelMap(PathFinder<Vertex> map)
    {
        // Override. Ideally this should be a callback
        // but I didn't want to contemplate over the best signature
        // for the delegate at the time.
        map.CostOverrides = StepOverrides;

        // Traverse the map with
        // current config options
        // TODO: Get the actual path taken
        map.Dijkstra_Shortest(out List<Vertex> path, out int steps);

        // Cache for easy access
        CachedTravelCost = steps;

        // Return
        return CachedTravelCost;
    }

    // Sets override for a terrain type
    public void SetOverride(string terrain, int cost)
    {
        StepOverrides[terrain] = cost;
    }
}*/