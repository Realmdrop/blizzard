﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml;
using TestConsoleApp.MazeProblem;

namespace TestConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // Problem 1
            Console.WriteLine("PROBLEM 1__________");
            uint input1 = 48778584;
            if (AllDigitsUnique(input1))
            {
                // Failed
                throw new Exception();
            }
            Console.WriteLine($"{input1} was not unique");

            input1 = 17308459;
            if (!AllDigitsUnique(input1))
            {
                // Passed
                throw new Exception();
            }
            Console.WriteLine($"{input1} was unique");
            Console.WriteLine();

            // Problem 2
            Console.WriteLine("PROBLEM 2__________");

            var expectedOutput2 = "     iiisssseeettttttlgorrrnnhh";
            var input2 = "sort the letters in this string";
            var sortPatten = " isetlgornh";
            var output = SortLetters(input2, sortPatten);

            if (output != expectedOutput2)
            {
                throw new Exception();
            }
            Console.WriteLine($"Input: {input2} with pattern: {sortPatten}");
            Console.WriteLine($"Output: '{output}'");
            Console.WriteLine();

            //  Problem 3
            Console.WriteLine("PROBLEM 3__________");

            // My rooms are a QWERTY keyboard.
            // use that for reference.
            // Could have made a file for this but I was too lazy to create a parser for this.
            Console.WriteLine("Creating a map of QWERTY keyboard.");
            Console.WriteLine("Connected Route1: QWERTY");
            Console.WriteLine("Connected Route2: A0ZXCV");
            Console.WriteLine("Connected Route3: VCXZAQ");

            Room roomStart = new Room("Q");

            // These should all come back with valid values.
            Room roomQ = new Room("Q");

            // Quick test for uniqueness
            if (roomQ.Name == roomStart.Name)
            {
                throw new Exception();
            }
            // Small bug here in garbage collection.
            // The value is still in World.Rooms.
            // We'll need a manual destruction or something but
            // ignoring for now
            roomQ = roomStart;

            // Full path. We are going to P
            Room roomW = new Room("W");
            Room roomE = new Room("E");
            Room roomR = new Room("R");
            Room roomT = new Room("T");
            Room roomY = new Room("Y");
            Room roomU = new Room("U");

            // Build relations
            // Made api return vertex just so this attachement was easy on eyes.
            roomStart.ConnectRoom(roomW).ConnectRoom(roomE).ConnectRoom(roomR).ConnectRoom(roomT).ConnectRoom(roomY).ConnectRoom(roomU);

            // Rooms we can't reach
            Room roomZ = new Room("Z");
            Room roomX = new Room("X");
            Room roomC = new Room("C");
            Room roomV = new Room("V");
            // Build relations
            roomZ.ConnectRoom(roomX).ConnectRoom(roomC).ConnectRoom(roomV);
            roomV.ConnectRoom(roomC).ConnectRoom(roomX).ConnectRoom(roomZ);

            // Allows V to reach Q but directions should prevent it.
            Room roomA = new Room("A");
            roomZ.ConnectRoom(roomA).ConnectRoom(roomStart);

            if (!roomStart.GetShortestDistance(roomU, out int steps))
            {
                throw new Exception();
            }


            // Write test

            Console.WriteLine("Q To U: " + steps);
            if (!roomStart.GetShortestDistance(roomE, out steps))
            {
                throw new Exception();
            }
            Console.WriteLine("Q To E: " + steps);
            if (roomStart.GetShortestDistance(roomV, out steps))
            {
                // No path should have been found.
                throw new Exception();
            }
            else if (steps != int.MaxValue)
            {
                // Max Val here indicates no path found
                throw new Exception();
            }
            Console.WriteLine("Q To V: No path found.");

            if (roomV.GetShortestDistance(roomStart, out steps))
            {
                Console.WriteLine("V to Q: " + steps);
            }

        }

        private static string SortLetters(string input, string sortOrder)
        {
            // Assumption:
            // sortOrder has an entry for every char in input
            // sortOrder has no duplicate chars.
            // Case sensitivity??? Ignored here.


            Dictionary<char, int> dict = new Dictionary<char, int>();

            // First build our dictionary
            foreach (var c in input)
            {
                if (!dict.ContainsKey(c))
                {
                    dict.Add(c, 1);
                    continue;
                }
                dict[c]++;
            }

            // Sort with the input string
            char[] ordered = new char[input.Length];
            int i = 0;
            foreach (var c in sortOrder)
            {
                // Input string never had the character.
                if (!dict.ContainsKey(c))
                {
                    continue;
                }

                var count = dict[c];
                for (int innerIndex = 0; innerIndex < count; innerIndex++)
                {
                    ordered[i] = c;
                    i++;
                }
            }

            // Due to assumptions, we do not check
            // whether we obtained input strings not
            // in sort order. They are skipped.
            // The problem with this is that
            // our final string's length will be shorter
            // than the original with padding at the end.
            // We can trim it, but that would cause other problems.

            return new string(ordered);
        }

        private static bool AllDigitsUnique(uint number)
        {
            bool[] digits = new bool[10];

            while (number > 0)
            {
                var val = number % 10;
                number = number / 10;

                // Console.WriteLine(val);
                if (digits[val])
                {
                    return false;
                }

                digits[val] = true;
            }

            return true;
        }

        /*     private static void GraphParse(string[] args)
             {
                 // Grab file. Use default if not provided.
                 string inputFile = args.Length == 0 ? "TestData.xml" : args[0];

                 // Parse and Validate
                 if (!XmlHelper.LoadXml(inputFile, out List<Player> players, out Map<Vertex> map))
                 {
                     Console.WriteLine("Could not parse file. Abort.");
                     return;
                 }

                 // Helpers
                 IEnumerable<Player> winners = new List<Player>();
                 bool busy = true;
                 var passedTime_ms = 0;
                 var timeout_ms = 5 * 60 * 1000;

                 // Create thread
                 var tr = new Thread(delegate ()
                 {
                     // Make each player travel the map
                     // and acquire the least steps taken.
                     int leastStepsTaken = players.Min(x => x.TravelMap(map));
                     // Grab all players with the min steps
                     winners = players.Where(x => x.CachedTravelCost == leastStepsTaken);
                     busy = false;
                 });

                 // Start thread
                 tr.Start();

                 // Notify busy
                 while (busy && passedTime_ms <= timeout_ms)
                 {
                     Console.WriteLine("calculating...");
                     Thread.Sleep(1000);
                     passedTime_ms += 1000;
                 }

                 // Still busy
                 if (busy)
                 {
                     Console.WriteLine("Operation timed out");
                     return;
                 }

                 // Print winners.
                 Console.WriteLine();
                 Console.WriteLine("Winners:");
                 foreach (var item in winners)
                 {
                     Console.WriteLine(string.Format("{0} - StepsTaken: {1}", item.Name, item.CachedTravelCost));
                 }
             }*/
    }

}