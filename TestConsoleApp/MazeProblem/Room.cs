﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestConsoleApp.MazeProblem
{
    class Room : Vertex
    {

        public string Name { get; private set; }

        public int MaxDoors { get; set; } = 4;

        public bool SetName(string newName)
        {
            // Not allowed
            if (string.IsNullOrWhiteSpace(newName))
            {
                return false;
            }

            if (Name == newName) { return true; }

            if (World.UsedNames.ContainsKey(newName) && World.UsedNames[newName] == true)
            {
                // Name already in use.
                return false;
            }

            // No thread safety here.
            World.UsedNames[newName] = true;
            if (!string.IsNullOrWhiteSpace(Name))
            {
                World.UsedNames[Name] = false;
            }
            Name = newName;
            return true;
        }

        public Room(string name)
        {
            // Let the world know about us
            World.Rooms.Add(this);

            // Set random name if bad name given
            if (!SetName(name))
            {
                SetName(Guid.NewGuid().ToString());
            }
        }
        public Room()
        {
            World.Rooms.Add(this);
            SetName(Guid.NewGuid().ToString());
        }

        public Room ConnectRoom(Room roomToConnect)
        {
            if (NeighborCount == MaxDoors)
            {
                // We've hit our max.
                // Deny request
                return null;
            }

            // Room is essentially a vertex
            // Created a base class so my brain
            // doesn't get confused between
            // functionalities of a door vs vertex.
            // And so it's easier for you guys to read.
            base.AddNeighbor(roomToConnect);
            return roomToConnect;
        }

        public bool RemoveRoom(Room roomToConnect)
        {
            // To do
            return false;
        }

        public bool GetShortestDistance(Room room, out int steps)
        {
            // Generate a pathfinder.
            PathFinder<Room> map = new PathFinder<Room>();

            // Assign start and end.
            map.Start = this;
            map.End = room;

            // Probably have no need to duplicate this
            // but did so just so we have a separate list
            // set. Thoughts of threading but not giving it much heed here.
            map.knownVertices = new List<Room>(World.Rooms);

            // I had this guy sitting on my comp
            // So grabbed it and altered the code
            // for this problem.
            // Workflow was a bit dif. in that problem
            // so it was a bigger headache to modify the existing
            // then write a new one :S
            return map.Dijkstra_Shortest(out List<Room> path, out steps);
        }

    }
}
