/*
using System.Collections.Generic;
using System.Linq;

// Simple graph class
public class Graph<T> where T:Vertex
{
    // All vertices
    public List<T> Vertices = new List<T>();

    // size
    public int Size => Vertices?.Count ?? 0;

    // Adds a pair of vertices as neighbors
    public void AddPair(T a, T b)
    {
        AddToList(a);
        AddToList(b);
        AddNeighbors(a, b);
    }

    // Adds single vertex. Not really needed here
    public void AddSingle(T x)
    {
        AddToList(x);
    }

    // Adds a vertex to list.
    // Nulls and walls are ignored
    private void AddToList(T a)
    {
        // If null or wall, exit
        if (a == null || a.TravelCost == "*") { return; }

        // If existing, exit
        if (Vertices.Contains(a)) { return; }

        // Add
        Vertices.Add(a);
    }

    // Adds a 1-away link for neighboring vertices
    private void AddNeighbors(T a, T b)
    {
        AddNeighbor(a, b);
      //  AddNeighbor(b, a);
    }

    // Gives vertex A a neighbor of B
    // Nulls are ignored.
    private void AddNeighbor(T a, T b)
    {
        if (b != null)
        {
            a?.AddNeighbor(b);
        }
    }

}*/