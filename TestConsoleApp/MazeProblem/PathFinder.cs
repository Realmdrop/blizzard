using System.Collections.Generic;
using System.Linq;
using System;

public class PathFinder<T> where T : Vertex, new()
{
    /// Graph structure
    public List<T> knownVertices = new List<T>();

    // Weight overrides. Default weight =1
    public Dictionary<string, int> CostOverrides = new Dictionary<string, int>();

    // The Start of map
    public T Start { get; set; }

    // The End/Destination of Map
    public T End { get; set; }

    // Gets an existing or creates a new vertex
    // for a given graph location
    private T GetOrCreate(int row, int col)
    {
        // If one was added,
        // use the existing
        var existing = knownVertices.FirstOrDefault(x => x.x == row && x.y == col);
        if (existing != null) { return existing; }


        // Create the vertex;
        T v = new T
        {
            x = row,
            y = col
        };

        return v;
    }

    // Map of distances to each verteex
    private Dictionary<Vertex, int> distances = null;

    // List of unvisited vertices
    List<Vertex> unvisitedVertices = null;

    // Uses Djikstra's algorithm to find shortest distance.
    public bool Dijkstra_Shortest(out List<T> path, out int steps)
    {

        // Invalid map
        if (Start == null || End == null)
        {
            // No path found.
            Console.WriteLine("No Start and/or End found");
            path = null;
            steps = -1;
            return false;
        }

        // Init
        #region Init

        // Clean start on variables
        distances = new Dictionary<Vertex, int>();
        unvisitedVertices = new List<Vertex>(knownVertices);

        // Distance to start is 0
        distances.Add(Start, 0);

        foreach (var v in unvisitedVertices)
        {
            // Set default max
            if (v != Start)
            {
                // We are using max as infinity.
                // This can cause overflows when adding so
                // we need to watch for this later.
                distances.Add(v, int.MaxValue);
            }
        }

        #endregion

        // Maybe want a timeout just in case?
        // while we have vertices pending,
        // loop
        while (unvisitedVertices.Count() != 0)
        {
            // get node with min distance
            // which we have never visited.
            var v = GetUnvisitedMin();

            // Console.WriteLine("Current Node [ {0} , {1}] = {2} | Dist: {3}", v.x, v.y, v.Terrain, distances[v]);

            if (v == null) { break; }
            unvisitedVertices.Remove(v);
            v.IsVisited = true;

            foreach (var neighbor in v.Neighbors)
            {
                // If for whatever reason,
                // our v distance is max,
                // attempting to add anything to it
                // will get us a negative.
                // This shouldn't happen anymore as I fixed the underlying
                // issue, but just in case GetCost() used a delegate or was changed.
                // Essentially if the max = infinity
                // max + n = infinity
                // So skip the node.
                if (distances[v] == int.MaxValue) { continue; }

                // Debug statements because something
                // had gone teerribly wrong.
                // Console.WriteLine("----NeighborNode" + neighbor.Terrain);
                // Console.WriteLine("-------Start-current" + distances[v]);
                // Console.WriteLine("-------current-neighbor" + GetCost(neighbor));
                // Console.WriteLine("-------Start-neighbor" + (distances[v] + GetCost(neighbor)));

                // Get new route
                var alternateRoute = distances[v] + GetCost(neighbor);

                // If new is better than existing,
                // save it
                if (alternateRoute < distances[neighbor])
                {
                    // Console.WriteLine("{0}-{1} is now {2}", v.Terrain, neighbor.Terrain, alternateRoute);
                    distances[neighbor] = alternateRoute;
                }
            }
        }

        // TODO. Keep track of chosen path
        // This way our return value will be more useful
        path = new List<T>();

        // Acquire distance
        steps = distances[End];
        return steps != int.MaxValue;
    }

    private Vertex GetUnvisitedMin()
    {
        Vertex minNode = null;
        foreach (var node in unvisitedVertices)
        {
            // one-time init
            if (minNode == null)
            {
                minNode = node;
                continue;
            }

            var dist = distances[node];
            if (dist < distances[minNode])
            {
                minNode = node;
            }
        }

        return minNode;
    }

    private int GetCost(Vertex neighbor)
    {
        // In the event each door has a different distance
        if (!CostOverrides.ContainsKey(neighbor.Terrain))
        {
            return 1;
        }

        return CostOverrides[neighbor.Terrain];
    }

    private Vertex GetMinDistance()
    {
        int min = distances.Min(x => x.Value);
        return distances.FirstOrDefault(x => x.Value == min).Key;
    }
}


