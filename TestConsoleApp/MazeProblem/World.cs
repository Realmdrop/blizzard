﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestConsoleApp.MazeProblem
{
    static class World
    {
        // Just to keep track of the names
        // Ideally need a better api to access and modify
        // but good enough for now
        public static Dictionary<string, bool> UsedNames = new Dictionary<string, bool>();

        public static List<Room> Rooms = new List<Room>();

    }
}
