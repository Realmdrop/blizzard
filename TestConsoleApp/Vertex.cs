
using System.Collections.Generic;
using System.Linq;

// Custom vertex
public class Vertex
{
    // Neighbors
    public IReadOnlyList<Vertex> Neighbors => neighbors;
    public List<Vertex> neighbors = new List<Vertex>();

    // Whether item was visited or not
    // Didn't used this in the end.
    public bool IsVisited { get; set; }

    // X pos in map.
    // Sorta like a composite key/id
    // because x,y positions should not be duplicated in map.
    // Technically not entirely needed, but
    // we need a way to identify which node related
    // to the map position to build the relationships.
    // Literally took me forever to find out
    // the lack of an id was causing all my problems
    public int x;

    // Y pos in map.
    // Sorta like a composite key/id
    // because x,y positions should not be duplicated in map.
    // Technically not entirely needed, but
    // we need a way to identify which node related
    // to the map position to build the relationships.
    // Literally took me forever to find out
    // the lack of an id was causing all my problems
    public int y;

    // Not needed
    public int NeighborCount => Neighbors?.Count ?? 0;

    // Used for step cost
    public string Terrain { get; set; } = "";

    // Creates 1-way link between nodes.
    protected Vertex AddNeighbor(Vertex v)
    {
        if (neighbors.Contains(v)) { return v; }
        neighbors.Add(v);
        return v;
    }

}
